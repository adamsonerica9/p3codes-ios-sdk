## Requirements

- iOS >= 11.0

## Integration using Cocoapods

PayPoSDK for iOS is available through CocoaPods. To install it, simply add the following line to your Podfile:

```bash
pod "PayPoSDK"
```

## Usage example

If you use Cocoapods use first:

```swift
import PayPoSDK
```

In next step use code:

```swift
func showPayPoFlow(url: String) {
    let payPoWebViewController = PayPoWebViewController(transactionUrl: url)
    payPoWebViewController.modalPresentationStyle = .popover
    self.present(payPoWebViewController, animated: true, completion: nil)
    
    payPoWebViewController.didFinishWithSucces = {
        payPoWebViewController.dismiss(animated: true) {
            // do some staff
        }
    }
    
    payPoWebViewController.didFinishWithFailure = {
        payPoWebViewController.dismiss(animated: true) {
            // do some staff
        }
    }
    
    payPoWebViewController.didClickCloseButton = {
        payPoWebViewController.dismiss(animated: true) {
            // do some staff
        }
    }
}
```
